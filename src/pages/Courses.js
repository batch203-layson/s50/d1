import CourseCard from "../components/CourseCard";

import coursesData from "../data/coursesData";

export default function Courses()
{
    console.log(coursesData);

    console.log(coursesData[0]);

    // The curly braces ({}) are used for props to singnify that we are providing information using javaScript expressions

    //Everytime the map method loops through the data, it creates "CourseCard" component and then passes the current element in our coursesData array using courseProp
    const courses = coursesData.map(course => {
        return(
            <CourseCard key={course.id} courseProp={course}/>
        );
    });

    return(
        <>
            <h1>Courses</h1>
            {courses}
            

        </>
    )

}

/* import Banner from "../components/Banner";
import Highlights from "../components/Highlights";
import { Row, Col, Button } from "react-bootstrap";

export default function Courses() {
    return (
        <Row>
            <Col className="p-5 ">
                <h1>Sample Coursed</h1>
                <h3>Description</h3>
                <p>Sample Description</p>
                <h3>Price</h3>
                <p>Php 50000</p>
                <Button variant="primary">Enroll Now!</Button>
            </Col>
        </Row>
                
          
    )
} */

