import {useState} from "react";

import {Card, Button} from "react-bootstrap";
                            //destructure the courseProp from the prop parameter
export default function CourseCard({courseProp})
{
    // console.log(props.courseProp.name);
    // console.log(typeof props);
   //console.log(courseProp);

//Scenario: Keep track the number of enrollees of each course.

// Destructure the course properties into their own variables
    const {name, description, price} = courseProp;

    // syntax
    // const [stateName, setStateName] = useState(initialStateValue);
    // using the state hook, it returns an array with the following elemnts:
        // first element contains the current initial state value 
        // second element is a function that is used to change the first element value of the first element
        const [count, setCount] = useState(0);
        console.log(useState(10));

    return(

        <Card className="my-3">
            <Card.Body>
                <Card.Title>
                    {name}
                </Card.Title>
                    <Card.Subtitle>
                        Description
                    </Card.Subtitle>
                        <Card.Text>
                            {description}
                        </Card.Text>
                <Card.Subtitle>
                    Price:
                </Card.Subtitle>
                <Card.Text>
                    Php {price}
                </Card.Text>
                <Button>Enroll Now</Button>


            </Card.Body>
        </Card>
    )
}